import numpy as np

def sum_sp():
    prev_total = np.float32(-1.0)
    curr_total = np.float32(0.0)
    one = np.float32(1.0)
    i = 0
    while curr_total != prev_total:
        prev_total = curr_total
        i += 1
        curr_total += one/i
    print(i, curr_total)
    return curr_total

if __name__ == '__main__':
    sum_sp = sum_sp()
