#include <iostream>
#include <limits>
#include <cstdio>

using namespace std;

template<class T>
T spsum() {
    T sum = 0.0, tmp = -1.0;
    long long i;
    int j = 0;
    T t = 1.0;
    T one = 1;
    for(i=1; sum != tmp; i++) {
        tmp = sum;
        t = one / i;
        sum += t;
        if ( ++j%100 == 0) {
            printf("%lld\t%.12f\t%g\n", i, sum, t);
            j++;
        }
        
    }
    printf("%lld\t%.12g\t%g\n", i, sum, t);
    cout << i << ' ' << (1.0 / i) << endl;
    cout << (i+1) << ' ' << (1.0 / (i+1)) << endl;
    cout << (i*(i+1)) << ' ' << (1.0 / (i*(i+1))) << endl;
    return sum;
}

int main(int argc, char* argv[]) {
    spsum<double>();
    cout << numeric_limits<double>::min() << endl;
    cout << numeric_limits<long long>::max() << endl;
}
