#!/usr/bin/env python
import argparse
import numpy as np
from numpy import linalg

def lufact(a):
    n = a.shape[0]
    m = np.identity(n)

    for k in range(0, n-1):

        if a[0][0] == 0:
            break

        for i in range(k+1, n):
            m[i][k] = a[i][k]/a[k][k]

        for j in range(k+1, n):
            for i in range(k+1, n):
                a[i][j] = a[i][j] - m[i][k] * a[k][j]

        print('-'*20)
        print("k = {}".format(k))
        print("m =")
        print(m)
        print("a =")
        print(a)

def lu_solve(A, b):
    pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LU Solver Demo')
    parser.add_argument('in_matrix', help='an input matrix file')
    args = parser.parse_args()

    A = np.loadtxt(args.in_matrix)
    lufact(A)
