#!/usr/bin/env python
import argparse
import numpy as np
from numpy import linalg

def lufact(a):
    n = a.shape[0]

    for k in range(0, n-1):

        if a[k][k] == 0:
            break

        for i in range(k+1, n):
            a[i][k] = a[i][k]/a[k][k]

        for j in range(k+1, n):
            for i in range(k+1, n):
                a[i][j] = a[i][j] - a[i][k] * a[k][j]

        print('-'*20)
        print("k = {}".format(k))
        print("a =")
        print(a)

def lusolve(A, b):
    n = b.shape[0]
    y = np.zeros(n)

    # forward subtitution for solving L*y = b
    y[0] = b[0]
    for i in range(1,n):
        psum = 0
        for j in range(i):
            psum += A[i][j] * y[j]
        y[i] = b[i] - psum

    print(y)

    # backward substitution for solving Ux = y
    x = np.zeros(n)
    for j in reversed(range(n)):
        if A[j][j] == 0:
            break;
        x[j] = y[j] / A[j][j]
        for i in range(j):
            y[i] = y[i] - A[i][j]*x[j]

    print(x)
    return x

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LU Solver Demo')
    parser.add_argument('indata', help='an input matrix file')
    args = parser.parse_args()

    with open(args.indata, "r") as f:
        line = f.readline().rstrip()
        n = int(line.split()[0])
    A = np.loadtxt(args.indata, skiprows=1)
    b = np.array([A[i][n] for i in range(n)])
    print(A)
    print(b)
    lufact(A)
    lusolve(A, b)


