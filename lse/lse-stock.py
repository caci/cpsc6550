#!/usr/bin/env python
import argparse
import numpy as np
from numpy import linalg

def stock():
    A = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [1, -1, 0], [1, 0, -1]]);
    y = np.array([10, 20, 30, -8, -23])
    x = linalg.lstsq(A, y)[0]
    print(x)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LSE Demo')
    args = parser.parse_args()
    stock()
