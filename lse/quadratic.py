#!/usr/bin/env python
import argparse
import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt

def stock():
    A = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [1, -1, 0], [1, 0, -1]]);
    y = np.array([10, 20, 30, -8, -23])
    x = linalg.lstsq(A, y)[0]
    print(x)

def plot_data(x, y):
    plt.plot(x, y, 'bo')
    plt.axis([min(x), max(x), min(y), max(y)])
    plt.show()




if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LSE Demo')
    args = parser.parse_args()
    xy = np.loadtxt('quadratic.dat', skiprows=1)
    x = xy[:,0]
    y = xy[:,1]
    print(x)
    print(y)
    #plot_data(x, y)
    A = np.vander(x, 3, increasing=True)
    print(A)
    c = linalg.lstsq(A, y)[0]
    print(c)
    xfit = np.linspace(min(x), max(x), num=50)
    yfit = [c[0] + c[1]*t + c[2]*t*t for t in xfit]
    plt.plot(x, y, 'ro', xfit, yfit, 'b-')
    plt.show()

