#!/usr/bin/env python
import argparse
import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt

def stock():
    A = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1], [1, -1, 0], [1, 0, -1]]);
    y = np.array([10, 20, 30, -8, -23])
    x = linalg.lstsq(A, y)[0]
    print(x)

def plot_data(x, y):
    plt.plot(x, y, 'b-')
    plt.axis([0, 1, 0, 1])
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LSE Demo')
    args = parser.parse_args()
    xy = np.loadtxt('planet.dat', skiprows=1)
    x = xy[:,0]
    y = xy[:,1]
    print(x)
    print(y)
    plot_data(x, y)
