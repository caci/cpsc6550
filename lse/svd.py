#!/usr/bin/env python
import argparse
import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt

def svd():
    A = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]]);
    u, s, v = linalg.svd(A)
    np.set_printoptions(precision=3)
    print(u)
    print(s)
    print(v)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='LSE Demo')
    args = parser.parse_args()
    svd()
