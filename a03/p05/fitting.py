import numpy as np
from numpy import linalg
import matplotlib.pyplot as plt

def polyf(c, t):
    val = 0
    tp = 1
    for i in range(len(c)):
        val += c[i] * tp;
        tp *= t
    return val

def fitting(x, y, order):
    #1. generate A
    A = np.vander(x, order+1, increasing = True)
    print(A)
    print(y)

    #2. solve lsq
    c = linalg.lstsq(A, y)[0]
    print(c)

    #3. create fit data
    xfit = np.linspace(min(x), max(x), num=200)
    yfit = [polyf(c, t) for t in xfit]

    #4 plot
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(x, y, 'ro', xfit, yfit, 'b-')
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$y$')
    ax.set_title('Data Fitting with {}-degree Polynomial'.format(order))
    fig.savefig('fitting-{}.png'.format(order))


if __name__ == '__main__':

    #1. read the data
    xy = np.loadtxt('Filip.dat', skiprows = 60)
    y = xy[:, 0]
    x = xy[:, 1]
    print(x)

    #2 fitting the data
    fitting(x, y, 4)
    fitting(x, y, 7)
    fitting(x, y, 10)



    

