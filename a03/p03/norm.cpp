//
//  main.cpp
//  a03p03
//
//  Created by Xizhou Feng on 4/18/16.
//  Copyright © 2016 Xizhou Feng. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <limits>
#include <chrono>
#include <omp.h>

using namespace std;
using namespace std::chrono;

double **create_matrix(int rows, int cols) {
    double *data = new double[rows*cols];
    double **mat = new double*[rows];
    mat[0] = data;
    for (int r = 1; r < rows; r++)
        mat[r] = mat[r-1] + cols;
    return mat;
}

double **create_test_matrix(int rows, int cols) {
    double **mat = create_matrix(rows, cols);
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            mat[r][c] = 1.0 * c;
        }
    }
    return mat;
}

double **read_matrix(const string& fname) {
    double **mat;
    ifstream is;
    is.open(fname.c_str());
    int rows, cols;
    is >> rows >> cols;
    mat = create_matrix(rows, cols);
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++)
            is >> mat[r][c];
    }
    is.close();
    return mat;
}

bool save_matrix(const string& fname, double **mat, int rows, int cols) {
    ofstream os;
    os.open(fname.c_str());
    os << rows << ' ' << cols << endl;
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            os << mat[r][c] << ' ';
        }
        os << endl;
    }
    os.close();
    return true;
}

double norm_1(double **m, int rows, int cols) {
    double *sums = new double[cols];
    for (int i = 0; i < cols; i++)
        sums[i] = 0.0;
    
    for (int c = 0; c < cols; c++) {
        for (int r = 0; r < rows; r++) {
            sums[c] += m[r][c];
        }
    }
    
    double maxv = numeric_limits<double>::min();
    for (int i=0; i <cols; i++) {
        if (sums[i] > maxv)
            maxv = sums[i];
    }
    return maxv;
}

double norm_1a(double **m, int rows, int cols) {
    double *sums = new double[cols];
    for (int i = 0; i < cols; i++)
        sums[i] = 0.0;
    
    for (int r = 0; r < rows; r++) {
        for (int c = 0; c < cols; c++) {
            sums[c] += m[r][c];
        }
    }
    
    double maxv = numeric_limits<double>::min();
    for (int i=0; i <cols; i++) {
        if (sums[i] > maxv)
            maxv = sums[i];
    }
    return maxv;
}


double norm_1b(double **m, int rows, int cols) {
    double *sums = new double[cols];
    int i;
#pragma omp parallel for private(i) firstprivate(cols) shared(sums)
    for (i = 0; i < cols; i++)
        sums[i] = 0.0;
    
    int r, c;
#pragma omp parallel for private(r, c, i) firstprivate(rows, cols) \
    shared(sums, m)
    for (c = 0; c < cols; c++) {
        for (r = 0; r < rows; r++) {
            sums[c] += m[r][c];
        }
    }
    
    double maxv = numeric_limits<double>::min();
#pragma omp parallel for private(i) firstprivate(cols) shared(sums) \
    reduction(max: maxv)
    for (i=0; i <cols; i++) {
        if (sums[i] > maxv)
            maxv = sums[i];
    }
    return maxv;
}

int main(int argc, const char * argv[]) {
    int rows = 1000, cols = 1000;
    if (argc >= 3) {
        rows = atoi(argv[1]);
        cols = atoi(argv[2]);
    }
    double **mat = create_test_matrix(rows, cols);
    //save_matrix("test.mat", mat, rows, cols);
    
    auto t = steady_clock::now();
    double norm = norm_1(mat, rows, cols);
    steady_clock::duration d = steady_clock::now() - t;
    cout << "norm = " << norm << " time = " << duration_cast<milliseconds>(d).count() << "ms" << endl;

    t = steady_clock::now();
    norm = norm_1a(mat, rows, cols);
    d = steady_clock::now() - t;
    cout << "norm = " << norm << " time = " << duration_cast<milliseconds>(d).count() << "ms" << endl;

    
    t = steady_clock::now();
    norm = norm_1b(mat, rows, cols);
    d = steady_clock::now() - t;
    cout << "norm = " << norm << " time = " << duration_cast<milliseconds>(d).count() << "ms" << endl;

    return 0;
}
