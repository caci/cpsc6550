import numpy as np
from scipy import linalg

if __name__ == '__main__':

    #1 read matrix
    mat = np.loadtxt('structure.mat', skiprows = 1)
    print(mat)

    n= mat.shape[0]
    A = mat[:, 0:n]
    b = mat[:, n]
    b = b.T

    print(A)
    print(b)

    #2 solve the system
    A = mat[:, 0:n]
    x = linalg.solve(A, b)
    print(x)

    #3 LU
    A = mat[:, 0:n]
    p, l, u = linalg.lu(A, permute_l = False)
    print(p)
    print(l)
    print(u)

    #4 svd
    A = mat[:, 0:n]
    U, s, Vh = linalg.svd(A)
    print(U)
    print(s)
    print(Vh)

    #5 eigvalues
    A = mat[:, 0:n]
    e, v = linalg.eig(A)
    print(e)
    print(max(e), min(e))
