#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

def f(x):
    return np.tan(x)

def f_diff_real(x):
    y = 1.0 / np.cos(x)
    return y * y

def f_diff_est(x, h):
    return 0.5 * (f(x+h) - f(x-h)) / h

def plot_he(h, e):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.loglog(h, e, basex=10, basey=10)
    fig.savefig("accuracy.png")

def main():
    x = 1
    v_real = f_diff_real(x)

    k = np.linspace(1.0, 16, 31)
    h = np.power(10, -k)
    e = np.zeros(len(h))

    for i in range(len(h)):
        v_est = f_diff_est(x, h[i])
        e[i] = np.fabs(v_est - v_real)/np.fabs(v_real)

    idx_min = np.argmin(e)
    logh = np.log10(h)
    loge = np.log10(e)

    lm1 = stats.linregress(logh[:idx_min-2], loge[:idx_min-2])
    lm2 = stats.linregress(logh[idx_min+2:], loge[idx_min+2:])

    print("Relative Discretization Error = {:.2f} h^{:.2f}".format(lm1[1], lm1[0]))
    print("Relative Rounding Error = {:.2f} h^{:.2f}".format(lm2[1], lm2[0]))

    e_lm = np.zeros(len(h))
    for i in range(len(h)):
        if i < idx_min - 2:
            e_lm[i] = np.power(10, lm1[1] + lm1[0] * logh[i])
        else:
            e_lm[i] = np.power(10, lm2[1] + lm2[0] * logh[i])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.loglog(h, e, 'ro', h, e_lm, 'b-', basex=10, basey=10)
    ax.set_xlabel('h')
    ax.set_ylabel('Relative Error')
    fig.savefig("accuracy.png")

if __name__ == '__main__':
    main()
